﻿using Microsoft.EntityFrameworkCore;
using ProjectStructure.DAL.Context;
using ProjectStructure.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectStructure.DAL
{
    public class GenericRepository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        private readonly ProjectContext _db;
        private readonly DbSet<TEntity> _dbSet;

        public GenericRepository(ProjectContext context)
        {
            _db = context ?? throw new ArgumentNullException(nameof(context));
            _dbSet = context.Set<TEntity>();
        }

        /// <summary>  
        /// generic method to fetch all the records from db  
        /// </summary>  
        /// <returns></returns>  
        public async Task<IEnumerable<TEntity>> GetAll() => await _dbSet.ToListAsync();

        /// <summary>  
        /// Generic get method on the basis of id for Entities.  
        /// </summary>  
        /// <param name="id"></param>  
        /// <returns></returns>  
        public async Task<TEntity> Get(object id) => await _dbSet.FindAsync(id);

        /// <summary>  
        /// generic Create method for the entities  
        /// </summary>  
        /// <param name="entity"></param>  
        public async Task<TEntity> Create(TEntity entity) => await Task.Run(() => _dbSet.Add(entity).Entity);

        /// <summary>  
        /// Generic update method for the entities  
        /// </summary>  
        /// <param name="entityToUpdate"></param>  
        public async Task Update(object id, TEntity entityToUpdate)
        {
            var trackedProject = await _dbSet.FindAsync(id);

            _db.Entry(trackedProject).CurrentValues.SetValues(entityToUpdate);
        }

        /// <summary>  
        /// Generic Delete method for the entities  
        /// </summary>  
        /// <param name="id"></param>  
        public async Task Delete(object id)
        {
            var entity = await _dbSet.FindAsync(id);

            if (entity != null)
            {
                _dbSet.Remove(entity);
            }
        }

        /// <summary>  
        /// generic Find method IEnumerable type for the entities  
        /// </summary>  
        /// <param name="where"></param>  
        /// <returns></returns>  
        public async Task<IEnumerable<TEntity>> FindIEnumerable(Func<TEntity, bool> predicate) => await Task.Run(() => _dbSet.Where(predicate).ToList());
    }
}
