﻿using ProjectStructure.DAL.Entities;
using System;
using System.Threading.Tasks;

namespace ProjectStructure.DAL.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        GenericRepository<Project> GetRepositoryProjects { get; }

        GenericRepository<Tasks> GetRepositoryTasks { get; }

        GenericRepository<Team> GetRepositoryTeams { get; }

        GenericRepository<User> GetRepositoryUsers { get; }

        Task Save();
    }
}
