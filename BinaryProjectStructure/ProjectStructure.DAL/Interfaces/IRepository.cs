﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectStructure.DAL.Interfaces
{
    public interface IRepository<T> where T : class
    {
        Task<IEnumerable<T>> GetAll();
        Task<T> Get(object id);
        Task<IEnumerable<T>> FindIEnumerable(Func<T, bool> predicate);
        Task<T> Create(T item);
        Task Update(object id, T item);
        Task Delete(object id);
    }
}
