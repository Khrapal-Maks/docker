﻿using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BLL.ModelsDTO;
using ProjectStructure.BLL.Services;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectStructure.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectsController : ControllerBase
    {
        private readonly IProjectService _projectService;

        public ProjectsController(IProjectService projectService)
        {
            _projectService = projectService;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<ProjectDTO>>> GetAllProjects()
        {
            return Ok(await _projectService.GetAllProjects());
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<IEnumerable<ProjectDTO>>> GetProject(int id)
        {
            var project = await _projectService.GetProject(id);

            if (project == null)
            {
                return NotFound();
            }

            return Ok(project);
        }

        [HttpPost]
        public async Task<ActionResult<ProjectDTO>> CreateProject([FromBody] NewProjectDTO newProject)
        {
            return Ok(await _projectService.CreateProject(newProject));
        }

        [HttpPut("{id}")]
        public async Task<ActionResult<ProjectDTO>> UpdateProject(int id, [FromBody] ProjectDTO project)
        {
            if (id != project.Id)
            {
                return BadRequest();
            }

            var projectToUpdate = await _projectService.GetProject(id);

            if (projectToUpdate == null)
            {
                return NotFound();
            }

            return Ok(await _projectService.UpdateProject(project));
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteProject(int id)
        {
            var projectToDelete = await _projectService.GetProject(id);

            if (projectToDelete == null)
            {
                return NotFound();
            }

            await _projectService.DeleteProject(id);
            return Ok();
        }
    }
}
