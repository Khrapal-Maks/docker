﻿using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BLL.ModelsDTO;
using ProjectStructure.BLL.Services;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectStructure.WebAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class ReportsController : ControllerBase
    {
        private readonly IReportsService _reportsService;
        private readonly IUsersService _usersService;

        public ReportsController(IReportsService reportsService, IUsersService usersService)
        {
            _reportsService = reportsService;
            _usersService = usersService;
        }

        [HttpGet("[action]/{id}")]
        public async Task<ActionResult<List<Tuple<ProjectDTO, int>>>> GetAllTasksInProjectsByAuthor(int id)
        {
            var user = await _usersService.GetUser(id);

            if (user == null)
            {
                return NoContent();
            }

            var result = await _reportsService.GetAllTasksInProjectsByAuthor(id);

            if (result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }

        [HttpGet("[action]/{id}")]
        public async Task<ActionResult<List<TasksDTO>>> GetAllTasksOnPerformer(int id)
        {

            var user = await _usersService.GetUser(id);

            if (user == null)
            {
                return NoContent();
            }

            var result = await _reportsService.GetAllTasksOnPerformer(id);

            if (result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }

        [HttpGet("[action]/{id}")]
        public async Task<ActionResult<List<Tuple<int, string>>>> GetAllTasksThatFinished(int id)
        {
            var user = await _usersService.GetUser(id);

            if (user == null)
            {
                return NoContent();
            }

            var result = await _reportsService.GetAllTasksThatFinished(id);

            if (result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }

        [HttpGet("[action]")]
        public async Task<ActionResult<List<Tuple<int, string, List<UserDTO>>>>> GetAllUsersOldestThanTenYears()
        {
            var result = await _reportsService.GetAllUsersOldestThanTenYears();

            if (result == null)
            {
                return NoContent();
            }

            return Ok(result);
        }

        [HttpGet("[action]")]
        public async Task<ActionResult<List<Tuple<string, List<TasksDTO>>>>> SortAllUsersFirstNameAndSortTaskOnName()
        {
            var result = await _reportsService.SortAllUsersFirstNameAndSortTaskOnName();

            if (result == null)
            {
                return NoContent();
            }

            return Ok(result);
        }

        [HttpGet("[action]/{id}")]
        public async Task<ActionResult<UserInfo>> GetStructUserById(int id)
        {
            var user = await _usersService.GetUser(id);

            if (user == null)
            {
                return NoContent();
            }

            var result = await _reportsService.GetStructUserById(id);

            if (result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }

        [HttpGet("[action]")]
        public async Task<ActionResult<List<ProjectInfo>>> GetStructAllProjects()
        {
            var result = await _reportsService.GetStructAllProjects();

            if (result == null)
            {
                return NoContent();
            }

            return Ok(result);
        }

        [HttpGet("[action]/{id}")]
        public async Task<ActionResult<List<TasksDTO>>> GetAllTasksInNotFinishedInWorkUser(int id)
        {
            var user = await _usersService.GetUser(id);

            if (user == null)
            {
                return NoContent();
            }

            var result = await _reportsService.GetAllTasksIsNotDoneByUser(id);

            if (result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }
    }
}
