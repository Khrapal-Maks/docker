﻿using ProjectStructure.DAL.Entities;
using System;
using System.Collections.Generic;

namespace ProjectStructure.BLL.ModelsDTO
{
    public struct ProjectInfo
    {
        public Project Project { get; set; }
        public Tasks TaskLongDescription { get; set; }
        public Tasks TasksShortName { get; set; }
        public int? CountUsersInTeamProject { get; set; }

        public override bool Equals(object obj)
        {
            return obj is ProjectInfo info &&
                   EqualityComparer<Project>.Default.Equals(Project, info.Project) &&
                   EqualityComparer<Tasks>.Default.Equals(TaskLongDescription, info.TaskLongDescription) &&
                   EqualityComparer<Tasks>.Default.Equals(TasksShortName, info.TasksShortName) &&
                   CountUsersInTeamProject == info.CountUsersInTeamProject;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Project, TaskLongDescription, TasksShortName, CountUsersInTeamProject);
        }

        public static bool operator ==(ProjectInfo left, ProjectInfo right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(ProjectInfo left, ProjectInfo right)
        {
            return !(left == right);
        }       
    }
}
