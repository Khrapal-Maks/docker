﻿using System;

namespace ProjectStructure.BLL.ModelsDTO
{
    public sealed class TeamDTO
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public DateTime CreatedAt { get; set; }

        public override bool Equals(object obj)
        {
            return obj is TeamDTO dTO &&
                   Id == dTO.Id &&
                   Name == dTO.Name &&
                   CreatedAt == dTO.CreatedAt;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Id, Name, CreatedAt);
        }
    }
}
