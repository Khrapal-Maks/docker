﻿namespace ProjectStructure.BLL.ModelsDTO
{
    public enum TaskStateDTO
    {
        IsCreate,

        IsProgress,

        IsTesting,

        IsDone
    }
}
