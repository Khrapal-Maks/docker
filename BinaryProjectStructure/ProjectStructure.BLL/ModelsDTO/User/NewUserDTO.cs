﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ProjectStructure.BLL.ModelsDTO
{
    public sealed class NewUserDTO
    {
        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Date)]
        public DateTime BirthDay { get; set; }
    }
}
