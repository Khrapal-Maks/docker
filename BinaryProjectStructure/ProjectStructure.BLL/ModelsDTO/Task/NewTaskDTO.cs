﻿using System.ComponentModel.DataAnnotations;

namespace ProjectStructure.BLL.ModelsDTO
{
    public sealed class NewTaskDTO
    {
        [Required]
        public int ProjectId { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Description { get; set; }
    }
}
