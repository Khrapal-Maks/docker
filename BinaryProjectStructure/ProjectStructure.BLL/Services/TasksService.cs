﻿using AutoMapper;
using ProjectStructure.BLL.ModelsDTO;
using ProjectStructure.DAL.Entities;
using ProjectStructure.DAL.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectStructure.BLL.Services
{
    public sealed class TasksService : BaseService, ITasksService
    {
        private readonly IUnitOfWork _database;

        public TasksService(IUnitOfWork context, IMapper mapper) : base(context, mapper)
        {
            _database = context;
        }

        public async Task<IEnumerable<TasksDTO>> GetAllTasks()
        {
            var tasks = await _database.GetRepositoryTasks.GetAll();

            return _mapper.Map<IEnumerable<TasksDTO>>(tasks);
        }

        public async Task<TasksDTO> GetTask(int id)
        {
            var task = await _database.GetRepositoryTasks.Get(id);

            return _mapper.Map<TasksDTO>(task);
        }

        public async Task<TasksDTO> CreateTask(NewTaskDTO newTask)
        {
            var taskEntity = _mapper.Map<Tasks>(newTask);

            var createToTask = await _database.GetRepositoryTasks.Create(taskEntity);
            await _database.Save();

            var createdTask = await _database.GetRepositoryTasks.Get(createToTask.Id);

            return _mapper.Map<TasksDTO>(createdTask);
        }

        public async Task<TasksDTO> UpdateTask(TasksDTO task)
        {
            var updateTask = _mapper.Map<Tasks>(task);

            await _database.GetRepositoryTasks.Update(task.Id, updateTask);

            await _database.Save();

            var updatedTask = await _database.GetRepositoryTasks.Get(task.Id);

            return _mapper.Map<TasksDTO>(updatedTask);
        }

        public async Task DeleteTask(int id)
        {
            await _database.GetRepositoryTasks.Delete(id);
            await _database.Save();
        }
    }
}
