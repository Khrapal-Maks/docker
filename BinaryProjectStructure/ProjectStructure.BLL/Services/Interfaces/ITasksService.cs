﻿using ProjectStructure.BLL.ModelsDTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectStructure.BLL.Services
{
    public interface ITasksService
    {
        Task<IEnumerable<TasksDTO>> GetAllTasks();

        Task<TasksDTO> CreateTask(NewTaskDTO item);

        Task<TasksDTO> GetTask(int value);

        Task<TasksDTO> UpdateTask(TasksDTO item);

        Task DeleteTask(int value);
    }
}
