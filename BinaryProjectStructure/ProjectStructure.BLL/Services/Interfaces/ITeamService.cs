﻿using ProjectStructure.BLL.ModelsDTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectStructure.BLL.Services
{
    public interface ITeamService
    {
        Task<IEnumerable<TeamDTO>> GetAllTeams();

        Task<TeamDTO> CreateTeam(NewTeamDTO item);

        Task<TeamDTO> GetTeam(int value);

        Task<TeamDTO> UpdateTeam(TeamDTO item);

        Task DeleteTeam(int value);
    }
}
