using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.AspNetCore.TestHost;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using ProjectStructure.BLL.ModelsDTO;
using ProjectStructure.DAL.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using Xunit;

namespace ProjectStructure.WebAPI.IntegrationTests
{
    public class ProjectsControllerTests
    {
        private readonly WebApplicationFactory<Startup> _factory;
        private HttpClient _client;

        public ProjectsControllerTests()
        {
            WebApplicationFactory<Startup> factory = new WebApplicationFactory<Startup>().WithWebHostBuilder(builder =>
            {
                builder.ConfigureTestServices(services =>
                {
                    var dbDescriptor = services.SingleOrDefault(d => d.ServiceType == typeof(DbContextOptions<ProjectContext>));

                    services.Remove(dbDescriptor);

                    services.AddDbContext<ProjectContext>(options =>
                    {
                        options.UseInMemoryDatabase("ProjectssTest");
                    });
                });
            });
            var dbContext = factory.Services.CreateScope().ServiceProvider.GetRequiredService<ProjectContext>();
            dbContext.Database.EnsureDeleted();
            Seed seed = new();
            dbContext.Projects.AddRange(seed.Projects);
            dbContext.SaveChanges();

            _factory = factory;
            _client = _factory.CreateClient();
        }

        [Fact]
        public async Task GetAllProjects()
        {
            //Arrange
            int projectsCountInDb = 30;

            //Act
            var response = await _client.GetAsync("/api/projects");
            response.EnsureSuccessStatusCode();

            var projectsCount = JsonConvert.DeserializeObject<List<ProjectDTO>>(await response.Content.ReadAsStringAsync()).Count;

            //Assert
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            Assert.Equal(projectsCountInDb, projectsCount);
        }

        [Fact]
        public async Task CreateProjects_ThanChecedNewProjectinDB_Positive()
        {
            //Arrange
            int shift = 1;
            var newProject = new NewProjectDTO()
            {
                AuthorId = 34,
                Name = "New project",
                Description = "New project",
                Deadline = DateTime.Now.AddMonths(3)
            };
            JsonContent content = JsonContent.Create(newProject);

            //Act
            var expectedCount = await _client.GetAsync("/api/projects");
            expectedCount.EnsureSuccessStatusCode();

            var projectsCountBefore = JsonConvert.DeserializeObject<List<ProjectDTO>>(await expectedCount.Content.ReadAsStringAsync()).Count;

            var expectedResponse = await _client.PostAsync("/api/projects", content);
            expectedResponse.EnsureSuccessStatusCode();
            var expected = JsonConvert.DeserializeObject<ProjectDTO>(await expectedResponse.Content.ReadAsStringAsync());

            var actualResponse = await _client.GetAsync("/api/projects/" + expected.Id);
            actualResponse.EnsureSuccessStatusCode();
            var actual = JsonConvert.DeserializeObject<ProjectDTO>(await actualResponse.Content.ReadAsStringAsync());

            var actualCount = await _client.GetAsync("/api/projects");
            actualCount.EnsureSuccessStatusCode();

            var projectsCountAfter = JsonConvert.DeserializeObject<List<ProjectDTO>>(await actualCount.Content.ReadAsStringAsync()).Count;

            //Assert
            Assert.Equal(HttpStatusCode.OK, actualResponse.StatusCode);
            Assert.Equal(expected, actual);
            Assert.Equal(projectsCountBefore + shift, projectsCountAfter);
        }

        [Fact]
        public async Task CreateProjects_ThanChecedNewProjectinDBWithInCorrectId_NotFound()
        {
            //Arrange
            int shift = 1;
            var newProject = new NewProjectDTO()
            {
                AuthorId = 34,
                Name = "New project",
                Description = "New project",
                Deadline = DateTime.Now.AddMonths(3)
            };
            JsonContent content = JsonContent.Create(newProject);

            //Act
            var expectedCount = await _client.GetAsync("/api/projects");
            expectedCount.EnsureSuccessStatusCode();

            var projectsCountBefore = JsonConvert.DeserializeObject<List<ProjectDTO>>(await expectedCount.Content.ReadAsStringAsync()).Count;

            var expectedResponse = await _client.PostAsync("/api/projects", content);
            expectedResponse.EnsureSuccessStatusCode();
            var expected = JsonConvert.DeserializeObject<ProjectDTO>(await expectedResponse.Content.ReadAsStringAsync());

            var actualResponse = await _client.GetAsync("/api/projects/" + (expected.Id + shift));
            var actual = JsonConvert.DeserializeObject<ProjectDTO>(await actualResponse.Content.ReadAsStringAsync());

            var actualCount = await _client.GetAsync("/api/projects");
            actualCount.EnsureSuccessStatusCode();

            var projectsCountAfter = JsonConvert.DeserializeObject<List<ProjectDTO>>(await actualCount.Content.ReadAsStringAsync()).Count;

            //Assert
            Assert.Equal(HttpStatusCode.NotFound, actualResponse.StatusCode);
            Assert.NotEqual(expected, actual);
            Assert.Equal(projectsCountBefore + shift, projectsCountAfter);
        }
    }
}
