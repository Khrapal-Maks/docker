﻿using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.AspNetCore.TestHost;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using ProjectStructure.BLL.ModelsDTO;
using ProjectStructure.DAL.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;

namespace ProjectStructure.WebAPI.IntegrationTests
{
    public class ReportsControllerTests
    {
        private readonly WebApplicationFactory<Startup> _factory;
        private readonly HttpClient _client;

        public ReportsControllerTests()
        {
            WebApplicationFactory<Startup> factory = new WebApplicationFactory<Startup>().WithWebHostBuilder(builder =>
            {
                builder.ConfigureTestServices(services =>
                {
                    var dbDescriptor = services.SingleOrDefault(d => d.ServiceType == typeof(DbContextOptions<ProjectContext>));

                    services.Remove(dbDescriptor);

                    services.AddDbContext<ProjectContext>(options =>
                    {
                        options.UseInMemoryDatabase("ReportssTest");
                    });
                });
            });
            var dbContext = factory.Services.CreateScope().ServiceProvider.GetRequiredService<ProjectContext>();
            dbContext.Database.EnsureDeleted();
            Seed seed = new();
            dbContext.Teams.AddRange(seed.Teams);
            dbContext.Users.AddRange(seed.Users);
            dbContext.Projects.AddRange(seed.Projects);
            dbContext.Tasks.AddRange(seed.Tasks);
            dbContext.SaveChanges();

            _factory = factory;
            _client = _factory.CreateClient();
        }

        [Fact]
        public async Task GetAllTasksInProjectsByAuthor_InCorrectIdUser_NoContent()
        {
            //Arrange
            int shift = 1;
            var users = await _client.GetAsync("/api/users");
            users.EnsureSuccessStatusCode();

            var incorrectId = JsonConvert.DeserializeObject<List<UserDTO>>(await users.Content.ReadAsStringAsync()).Max(x => x.Id) + shift;
            //Act
            var response = await _client.GetAsync("/api/reports/getalltasksinprojectsbyauthor/" + incorrectId);

            //Assert
            Assert.Equal(HttpStatusCode.NoContent, response.StatusCode);
        }

        [Fact]
        public async Task GetAllTasksInProjectsByAuthor_CorrectIdUser_IsResult()
        {
            //Arrange
            var projects = await _client.GetAsync("/api/projects");
            projects.EnsureSuccessStatusCode();

            var correctId = JsonConvert.DeserializeObject<List<ProjectDTO>>(await projects.Content.ReadAsStringAsync()).First().AuthorId;
            var project = JsonConvert.DeserializeObject<List<ProjectDTO>>(await projects.Content.ReadAsStringAsync()).First(x => x.AuthorId == correctId);

            var tasks = await _client.GetAsync("/api/tasks/");
            var tasksCount = JsonConvert.DeserializeObject<List<TasksDTO>>(await tasks.Content.ReadAsStringAsync()).FindAll(x => x.ProjectId == project.Id).Count;

            //Act
            var response = await _client.GetAsync("/api/reports/getalltasksinprojectsbyauthor/" + correctId);
            var actual = JsonConvert.DeserializeObject<List<Tuple<ProjectDTO, int>>>(await response.Content.ReadAsStringAsync());

            //Assert
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            Assert.Equal(project, actual.Select(x => x.Item1).First());
            Assert.Equal(tasksCount, actual.Select(x => x.Item2).First());
        }

        [Fact]
        public async Task GetAllTasksInNotFinishedInWorkUser_CorrectIdUser_IsResult()
        {
            //Arrange   
            var tasks = await _client.GetAsync("/api/tasks");
            tasks.EnsureSuccessStatusCode();

            var correctId = JsonConvert.DeserializeObject<List<TasksDTO>>(await tasks.Content.ReadAsStringAsync()).First().PerformerId;
            var expected = JsonConvert.DeserializeObject<List<TasksDTO>>(await tasks.Content.ReadAsStringAsync()).FindAll(x => x.PerformerId == correctId & x.FinishedAt == null);

            //Act
            var response = await _client.GetAsync("/api/reports/getalltasksinNotfinishedinworkuser/" + correctId);
            var actual = JsonConvert.DeserializeObject<List<TasksDTO>>(await response.Content.ReadAsStringAsync());

            //Assert
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            Assert.Equal(expected, actual);
            Assert.Equal(expected.Count, actual.Count);
        }

        [Fact]
        public async Task GetAllTasksInNotFinishedInWorkUser_InCorrectIdUser_NoContent()
        {
            //Arrange   
            int shift = 1;
            var users = await _client.GetAsync("/api/users");
            users.EnsureSuccessStatusCode();

            var incorrectId = JsonConvert.DeserializeObject<List<UserDTO>>(await users.Content.ReadAsStringAsync()).Max(x => x.Id) + shift;

            //Act
            var response = await _client.GetAsync("/api/reports/getalltasksinNotfinishedinworkuser/" + incorrectId);
            var actual = JsonConvert.DeserializeObject<List<TasksDTO>>(await response.Content.ReadAsStringAsync());

            //Assert
            Assert.Equal(HttpStatusCode.NoContent, response.StatusCode);
        }
    }
}
