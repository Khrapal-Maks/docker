﻿using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.AspNetCore.TestHost;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using ProjectStructure.BLL.ModelsDTO;
using ProjectStructure.DAL.Context;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using Xunit;

namespace ProjectStructure.WebAPI.IntegrationTests
{
    public class TeamControllerTests
    {
        private readonly WebApplicationFactory<Startup> _factory;
        private HttpClient _client;

        public TeamControllerTests()
        {
            WebApplicationFactory<Startup> factory = new WebApplicationFactory<Startup>().WithWebHostBuilder(builder =>
            {
                builder.ConfigureTestServices(services =>
                {
                    var dbDescriptor = services.SingleOrDefault(d => d.ServiceType == typeof(DbContextOptions<ProjectContext>));

                    services.Remove(dbDescriptor);

                    services.AddDbContext<ProjectContext>(options =>
                    {
                        options.UseInMemoryDatabase("TeamsTest");
                    });
                });
            });
            var dbContext = factory.Services.CreateScope().ServiceProvider.GetRequiredService<ProjectContext>();
            dbContext.Database.EnsureDeleted();
            Seed seed = new();
            dbContext.Teams.AddRange(seed.Teams);
            dbContext.SaveChanges();

            _factory = factory;
            _client = _factory.CreateClient();
        }

        [Fact]
        public async Task CreateTeam_ThanChecedNewTeaminDB_Positive()
        {
            //Arrange
            int shift = 1;
            var newTeam = new NewTeamDTO()
            {
                Name = "New Team"
            };
            JsonContent content = JsonContent.Create(newTeam);

            //Act
            var expectedCount = await _client.GetAsync("/api/team");
            expectedCount.EnsureSuccessStatusCode();

            var teamsCountBefore = JsonConvert.DeserializeObject<List<TeamDTO>>(await expectedCount.Content.ReadAsStringAsync()).Count;

            var expectedResponse = await _client.PostAsync("/api/team", content);
            expectedResponse.EnsureSuccessStatusCode();
            var expected = JsonConvert.DeserializeObject<TeamDTO>(await expectedResponse.Content.ReadAsStringAsync());

            var actualResponse = await _client.GetAsync("/api/team/" + expected.Id);
            actualResponse.EnsureSuccessStatusCode();
            var actual = JsonConvert.DeserializeObject<TeamDTO>(await actualResponse.Content.ReadAsStringAsync());

            var actualCount = await _client.GetAsync("/api/team");
            actualCount.EnsureSuccessStatusCode();

            var teamsCountAfter = JsonConvert.DeserializeObject<List<TeamDTO>>(await actualCount.Content.ReadAsStringAsync()).Count;

            _client.Dispose();

            //Assert
            Assert.Equal(HttpStatusCode.OK, actualResponse.StatusCode);
            Assert.Equal(expected, actual);
            Assert.Equal(teamsCountBefore + shift, teamsCountAfter);
        }

        [Fact]
        public async Task CreateTeam_ThanChecedNewTeaminDBWithInCorrectId_NotFound()
        {
            //Arrange
            int shift = 1;
            var newTeam = new NewTeamDTO()
            {
                Name = "New Team"
            };
            JsonContent content = JsonContent.Create(newTeam);

            //Act
            var expectedCount = await _client.GetAsync("/api/team");
            expectedCount.EnsureSuccessStatusCode();

            var teamsCountBefore = JsonConvert.DeserializeObject<List<TeamDTO>>(await expectedCount.Content.ReadAsStringAsync()).Count;

            var expectedResponse = await _client.PostAsync("/api/team", content);
            expectedResponse.EnsureSuccessStatusCode();
            var expected = JsonConvert.DeserializeObject<TeamDTO>(await expectedResponse.Content.ReadAsStringAsync());

            var actualResponse = await _client.GetAsync("/api/team/" + (expected.Id + shift));
            var actual = JsonConvert.DeserializeObject<TeamDTO>(await actualResponse.Content.ReadAsStringAsync());

            var actualCount = await _client.GetAsync("/api/team");
            actualCount.EnsureSuccessStatusCode();

            var teamsCountAfter = JsonConvert.DeserializeObject<List<TeamDTO>>(await actualCount.Content.ReadAsStringAsync()).Count;

            _client.Dispose();

            //Assert
            Assert.Equal(HttpStatusCode.NotFound, actualResponse.StatusCode);
            Assert.NotEqual(expected, actual);
            Assert.Equal(teamsCountBefore + shift, teamsCountAfter);
        }
    }
}
