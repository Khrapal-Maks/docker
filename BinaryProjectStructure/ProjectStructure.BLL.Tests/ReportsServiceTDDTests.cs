﻿using FakeItEasy;
using ProjectStructure.BLL.ModelsDTO;
using ProjectStructure.BLL.Services;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace ProjectStructure.BLL.Tests
{
    public class ReportsServiceTDDTests
    {
        private readonly List<TasksDTO> _tasksDTO = new()
        {
            new TasksDTO() { Id = 1, Name = "first", Description = "first", ProjectId = 1, CreatedAt = DateTime.Now, FinishedAt = null, PerformerId = 1, State = TaskStateDTO.IsProgress },
            new TasksDTO() { Id = 2, Name = "second", Description = "second", ProjectId = 2, CreatedAt = DateTime.Now, FinishedAt = null, PerformerId = 1, State = TaskStateDTO.IsProgress },
            new TasksDTO() { Id = 3, Name = "third", Description = "third", ProjectId = 2, CreatedAt = DateTime.Now, FinishedAt = null, PerformerId = 2, State = TaskStateDTO.IsProgress }
        };

        private readonly IReportsService _reportsService;

        public ReportsServiceTDDTests()
        {
            _reportsService = A.Fake<IReportsService>();
        }

        [Fact]
        public async Task GetAllTasks_IsNotDoneByUser()
        {
            //Arrange            
            int id = 1;
            A.CallTo(() => _reportsService.GetAllTasksIsNotDoneByUser(id)).Returns(_tasksDTO.FindAll(x => x.PerformerId == id));

            //Act
            var actual = await _reportsService.GetAllTasksIsNotDoneByUser(id);

            //Assert
            Assert.Equal(_tasksDTO.FindAll(x => x.PerformerId == id), actual);
        }
    }
}
