﻿using ProjectStructureClient.Services;
using System;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ProjectStructureClient
{
    public class ReportsMenu
    {
        private OperationService _operationService;

        public ReportsMenu()
        {
            _operationService = new();
            GetMenu().ContinueWith((t) => t.Start());
        }

        public async Task GetMenu()
        {
            _operationService = new();
            Console.OutputEncoding = Encoding.UTF8;
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("\n\n\n\n\n\n\t\t\t\t\t\tSelect Task\n");
            Console.ForegroundColor = ConsoleColor.DarkYellow;
            Console.WriteLine("\t\t\t    Name operation:                                  Select operation\n" +
                              "                                                                             and enter number. ");
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine(
                "\t\t\tTask 1 Get all tasks in projects by author                   1\n" +
                "\t\t\tTask 2 Get all tasks on performer                            2\n" +
                "\t\t\tTask 3 Get all tasks that finished at 2021 year              3\n" +
                "\t\t\tTask 4 Get all users oldest than 10 years                    4\n" +
                "\t\t\tTask 5 Sort all users First name and sort task on Name       5\n" +
                "\t\t\tTask 6 Get struct User by Id                                 6\n" +
                "\t\t\tTask 7 Get struct all projects                               7\n" +
                "\n" +
                "\t\t\tEnter:");

            await ValidationMenu();
        }

        private async Task ValidationMenu()
        {
            int selection;

            Console.SetCursorPosition(30, 18);
            while (!int.TryParse(Console.ReadLine(), out selection))
            {
                await Error();
            }
            await GetOperation(selection);
        }

        private async Task GetOperation(int selection)
        {
            switch (selection)
            {
                case 1:
                    await _operationService.GetAllTasksInProjectsByAuthor();
                    await ConsoleClearForMenu();
                    break;
                case 2:
                    await _operationService.GetAllTasksOnPerformer(); 
                    await ConsoleClearForMenu();
                    break;
                case 3:
                    await _operationService.GetAllTasksThatFinished();
                    await ConsoleClearForMenu();
                    break;
                case 4:
                    await _operationService.GetAllUsersOldestThanTenYears();
                    await ConsoleClearForMenu();
                    break;
                case 5:
                    await _operationService.SortAllUsersFirstNameAndSortTaskOnName();
                    await ConsoleClearForMenu();
                    break;
                case 6:
                    await _operationService.GetStructUserById();
                    await ConsoleClearForMenu();
                    break;
                case 7:
                    await _operationService.GetStructAllProjects();
                    await ConsoleClearForMenu();
                    break;                
                default:
                    Console.Clear();
                    await Error();
                    break;
            }
        }

        private async Task ConsoleClearForMenu()
        {
            Console.ReadKey();
            Console.Clear();
            await GetMenu().ContinueWith((x) => x.Start());
        }

        private async Task Error()
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Clear();
            Console.WriteLine("Input error, check if the input is correct!");
            await ConsoleClearForMenu();
        }
    }
}
